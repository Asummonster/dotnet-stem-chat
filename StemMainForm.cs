﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace STEMChat
{
    public partial class StemMainForm : Form
    {
        public StemMainForm()
        {
            InitializeComponent();
        }

        private void StemMainFormLoaded(object sender, EventArgs e)
        {
            ChatManager.OnNewMessage = (string message) =>
            {
                RichText.AppendText(message);
                RichText.ScrollToCaret();
            };
            Channels.Items.Add(new Channel("New channel", true));
            Channels.SelectedIndexChanged += (object sender, EventArgs e) =>
            {
                object Selected = Channels.SelectedItem;
                if (Selected is Channel Item)
                {
                    if (Item.New)
                    {
                        string ch = Prompt.ShowDialog("Subscribe to channel");
                        ch = ch.Substring(0, ch.Length < 255 ? ch.Length : 255);
                        Channels.SelectedIndex = Channels.Items.Add(new Channel(ch));
                        ChatManager.Channels[ch] = "";
                        ChatManager.SelectedChannel = ch;
                        RichText.Text = ChatManager.Channels[ch];
                        RichText.AppendText(" ");
                        RichText.Text = RichText.Text.Substring(0, RichText.Text.Length - 1);
                        ChatManager.Subscribe(ch);
                    }
                    else
                    {
                        if (ChatManager.Channels.ContainsKey(Item.ToString()))
                        {
                            ChatManager.SelectedChannel = Item.ToString();
                            RichText.Text = ChatManager.Channels[Item.ToString()];
                            RichText.AppendText(" ");
                            RichText.Text = RichText.Text.Substring(0, RichText.Text.Length - 1);
                        }
                    }
                }
            };
        }

        private void OnTextEntryKeyPressed(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string message = textBox1.Text;
                if (message.Length > 0 && ChatManager.SelectedChannel != null && ChatManager.Channels.ContainsKey(ChatManager.SelectedChannel))
                {
                    ChatManager.Send(ChatManager.SelectedChannel, message);
                    message = ">>" + message + "\n";
                    ChatManager.Channels[ChatManager.SelectedChannel] += message;
                    RichText.AppendText(message);
                    RichText.ScrollToCaret();
                    textBox1.Text = "";
                }
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }
        private void onRichTextKeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
            e.Handled = true;
            if ((e.Control && e.KeyCode == Keys.C) || (e.Control && e.KeyCode == Keys.Insert))
            {
                string text = RichText.SelectedText;
                text = text.EndsWith("\n") ? text.Substring(0, text.Length - 1) : text;
                Clipboard.SetText(text);
            }
            else if (e.Control && e.KeyCode == Keys.A)
            {
                RichText.SelectAll();
            }
        }

        [DllImport("user32.dll")]
        static extern bool HideCaret(IntPtr hWnd);
        private void RichText_HideCaret(object sender, EventArgs e)
        {
           HideCaret(RichText.Handle);
        }
        private void RichText_HideCaretMouse(object sender, MouseEventArgs e)
        {
           HideCaret(RichText.Handle);
        }

        private void StemMainFormClosing(object sender, FormClosingEventArgs e)
        {
            ChatManager.Connection.Disconnect();
        }
    }
}
