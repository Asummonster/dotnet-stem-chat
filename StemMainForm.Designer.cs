﻿
namespace STEMChat
{
    partial class StemMainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StemMainForm));
            this.Channels = new System.Windows.Forms.ListBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.RichText = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // Channels
            // 
            this.Channels.Dock = System.Windows.Forms.DockStyle.Left;
            this.Channels.FormattingEnabled = true;
            this.Channels.IntegralHeight = false;
            this.Channels.ItemHeight = 15;
            this.Channels.Location = new System.Drawing.Point(0, 0);
            this.Channels.Margin = new System.Windows.Forms.Padding(0);
            this.Channels.Name = "Channels";
            this.Channels.Size = new System.Drawing.Size(143, 559);
            this.Channels.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox1.Location = new System.Drawing.Point(143, 536);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(799, 23);
            this.textBox1.TabIndex = 1;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnTextEntryKeyPressed);
            // 
            // RichText
            // 
            this.RichText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RichText.Location = new System.Drawing.Point(143, 0);
            this.RichText.Margin = new System.Windows.Forms.Padding(0);
            this.RichText.Name = "RichText";
            this.RichText.ReadOnly = true;
            this.RichText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.RichText.Size = new System.Drawing.Size(799, 536);
            this.RichText.TabIndex = 2;
            this.RichText.Text = "";
            this.RichText.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichText_HideCaretMouse);
            this.RichText.Enter += new System.EventHandler(this.RichText_HideCaret);
            this.RichText.GotFocus += new System.EventHandler(this.RichText_HideCaret);
            this.RichText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onRichTextKeyDown);
            this.RichText.Validated += new System.EventHandler(this.RichText_HideCaret);
            // 
            // StemMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(942, 559);
            this.Controls.Add(this.RichText);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Channels);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StemMainForm";
            this.Text = "Stem";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StemMainFormClosing);
            this.Load += new System.EventHandler(this.StemMainFormLoaded);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox Channels;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RichTextBox RichText;
    }
}

