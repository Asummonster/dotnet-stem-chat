using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Stem;

namespace STEMChat
{
    internal delegate void ChannelAppender (string text);
    internal static class ChatManager
    {
        internal static ChannelAppender OnNewMessage { get; set; }
        internal static StemConnection Connection { get; set; }
        public static readonly Dictionary<string, string> Channels = new Dictionary<string, string>();
        public static string SelectedChannel;
        public static void Send(string channel, string message)
        {
            Connection.Send(channel, message);
        }
        public static void Subscribe(string channel)
        {
            Connection.Subscribe(channel);
        }
        public static void Unsubscribe(string channel)
        {
            Connection.Unsubscribe(channel);
        }
    }
    internal class Channel
    {
        public string Text;
        public bool New;

        public Channel(string text = "Chanel", bool _new = false)
        {
            New = _new;
            Text = text;
        }
        public override string ToString()
        {
            return Text;
        }
    }
    public static class Prompt
    {
        public static string ShowDialog(string caption)
        {
            Form prompt = new Form()
            {
                Width = 300,
                Height = 62,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = caption,
                StartPosition = FormStartPosition.CenterScreen
            };
            TextBox textBox = new TextBox() {};
            textBox.Dock = DockStyle.Fill;
            textBox.KeyDown += (object sender, KeyEventArgs e) =>
            {
                if (e.KeyCode == Keys.Enter)
                {
                    prompt.Close();
                }
            };
            prompt.Controls.Add(textBox);
            prompt.ShowDialog();
            return textBox.Text;
        }
    }
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ChatManager.Connection = new StemConnection();
            ChatManager.Connection.OnMessage += (string channel, string message) =>
            {
                message = "<<" + (message.EndsWith("\n") ? message : message + "\n");
                ChatManager.Channels[channel] += message;
                if (ChatManager.SelectedChannel == channel)
                {
                    ChatManager.OnNewMessage?.Invoke(message);
                }
            };
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new StemMainForm());
        }
    }
}
